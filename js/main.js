jQuery(document).ready(function($) {
	// видео 
	$('.video__btn').on('click', function (event) {
		event.preventDefault();
		$('.popup-video').fadeIn();
	});
	$('.popup-video-close').on('click', function (event) {
		event.preventDefault();
		$('.popup-video').fadeOut();
	});
	// скроллинг
	$('a[data-target^="scroll"]').bind('click.smoothscroll', function () {
		$('.mobile-nav').removeClass('mobile-active');
		var target = $(this).attr('href'),
			bl_top = $(target).offset().top;
		$('body, html').animate({
			scrollTop: bl_top
		}, 1000);
		return false;
	});

	$("#view").selectmenu();

	$('.ads-slider').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		dots: false,
		arrows: true,
		prevArrow: '<div class="ads-slider-arrows ads-slider-arrows-prev"></div>',
		nextArrow: '<div class="ads-slider-arrows ads-slider-arrows-next"></div>',
		responsive: [
			{
				breakpoint: 640,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 960,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}			
		]
	});
	

	var numSlick = 0;
	$('.object-slider-for').each(function () {
		numSlick++;
		$(this).addClass('slider-' + numSlick).slick({
			fade: false,
			dots: false,
			lazyLoad: 'progressive',
			arrows: false,
			slidesToScroll: 1,
			slidesToShow: 1,
			infinite: true,
			swipe: false,
			asNavFor: '.object-slider-nav.slider-' + numSlick
		});
	});
	numSlick = 0;
	$('.object-slider-nav').each(function () {
		numSlick++;
		$(this).addClass('slider-' + numSlick).slick({
			dots: false,
			centerMode: false,
			focusOnSelect: true,
			infinite: true,
			arrows: false,	
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.object-slider-for.slider-' + numSlick,
		});
	});
});